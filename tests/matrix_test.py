import math
from functools import reduce
from operator import add

import pytest

from pytracer.matrix import Matrix
from pytracer.tuple import Tuple


def test_matrix_data():
    data = [
        [1, 2, 3, 4],
        [5.5, 6.5, 7.5, 8.5],
        [9, 10, 11, 12],
        [13.5, 14.5, 15.5, 16.5],
    ]
    m = Matrix(data=data)

    assert m.size == 4
    assert m[0][0] == 1
    assert m[0][3] == 4
    assert math.isclose(m[1][0], 5.5)
    assert math.isclose(m[3][2], 15.5)


@pytest.mark.parametrize(
    "n",
    [
        pytest.param(4, id="size4"),
        pytest.param(3, id="size3"),
        pytest.param(2, id="size2"),
    ],
)
def test_default(n):
    print(type(n))
    m = Matrix(size=n)
    assert m.size == n
    assert reduce(add, m.values) == [0] * (n * n)


def test_equality():
    data = [
        [1, 2, 3, 4],
        [5.5, 6.5, 7.5, 8.5],
        [9, 10, 11, 12],
        [13.5, 14.5, 15.5, 16.5],
    ]

    m1 = Matrix(data=data)
    m2 = Matrix(data=data)
    assert m1 == m2

    other_data = [
        [1, 2, 3.4, 4.01],  # changed
        [5.5, 6.5, 7.5, 8.5],
        [9, 10, 11, 12],
        [13.5, 14.5, 15.5, 16.5],
    ]

    m1 = Matrix(data=data)
    m2 = Matrix(data=other_data)
    assert m1 != m2


def test_matrix_multiplication():
    # fmt: off
    data1 = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 8, 7, 6],
        [5, 4, 3, 2]
    ]

    other_data = [
        [-2, 1, 2, 3],
        [3, 2, 1, -1],
        [4, 3, 6, 5],
        [1, 2, 7, 8],
    ]

    m1 = Matrix(data=data1)
    m2 = Matrix(data=other_data)
    multiplied = m1 * m2

    expected = [
        [20, 22, 50, 48],
        [44, 54, 114, 108],
        [40, 58, 110, 102],
        [16, 26, 46, 42]
    ]

    assert Matrix(data=expected) == multiplied
    
    # def test_tuple_multiplication():
    #     data = [
    #         [1, 2, 3, 4],
    #         [2, 4, 4, 2],
    #         [8, 6, 4, 1],
    #         [0, 0, 0, 1]
    #     ]

    #     m = Matrix(data=data)
    #     t = Tuple(1, 2, 3, 1)

    #     assert m * t == Tuple(18, 24, 33, 1)

