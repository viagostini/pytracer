from pytracer.point import Point
from pytracer.tuple import Tuple


def test_point():
    a = Point(4.3, -4.2, 3.1)
    assert str(a) == "<Point (4.3, -4.2, 3.1)>"
    assert isinstance(a, Tuple)
    assert type(a) == Point
