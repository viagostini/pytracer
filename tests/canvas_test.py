from functools import reduce
from operator import add

from pytracer.canvas import Canvas
from pytracer.color import Color


def test_canvas():
    c = Canvas(10, 20)
    assert c.width == 10
    assert c.height == 20
    assert reduce(add, c.grid) == [Color(0, 0, 0)] * 200


def test_pixel_at():
    c = Canvas()
    black = Color(0, 0, 0)
    for i in range(c.height):
        for j in range(c.width):
            assert c.pixel_at(i, j) == black


def test_write_pixel():
    c = Canvas(10, 20)
    r = Color(1, 0, 0)
    c.write_pixel_at(2, 3, r)
    assert c.pixel_at(2, 3) == r
