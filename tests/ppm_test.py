from pytracer.canvas import Canvas
from pytracer.color import Color
from pytracer.ppm import PPMWriter


def test_ppm_header():
    c = Canvas(5, 3)
    header = PPMWriter.header(c)
    assert header == "P3\n5 3\n255\n"


def test_ppm_pixels():
    c = Canvas(5, 3)
    c1 = Color(1.5, 0, 0)
    c2 = Color(0, 0.5, 0)
    c3 = Color(-0.5, 0, 1)

    c.write_pixel_at(0, 0, c1)
    c.write_pixel_at(2, 1, c2)
    c.write_pixel_at(4, 2, c3)

    pixels = PPMWriter.pixels(c)
    expected = (
        "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n"
        "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0\n"
        "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255\n"
    )
    assert pixels == expected


def test_split_long_lines():
    c = Canvas(10, 2, Color(1, 0.8, 0.6))
    pixels = PPMWriter.pixels(c)
    expected = (
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
        "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
        "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
    )
    assert pixels == expected


def test_ppm():
    c = Canvas(10, 2, Color(1, 0.8, 0.6))
    ppm = PPMWriter.ppm(c)
    expected = (
        "P3\n10 2\n255\n"
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
        "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
        "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
    )
    assert ppm == expected


def test_endwith_newline():
    c = Canvas(10, 2, Color(1, 0.8, 0.6))
    ppm = PPMWriter.ppm(c)
    assert ppm.endswith("\n")
