from pytracer.color import Color


def test_color():
    c = Color(-0.5, 0.4, 1.7)
    assert str(c) == "<Color (-0.5, 0.4, 1.7)>"


def test_multiplication():
    c1 = Color(1, 0.2, 0.4)
    c2 = Color(0.9, 1, 0.1)
    assert c1 * c2 == Color(0.9, 0.2, 0.04)
