import pytest

from pytracer.color import Color
from pytracer.point import Point
from pytracer.tuple import Tuple
from pytracer.vector import Vector


def test_tuple():
    a = Tuple(4.3, -4.2, 3.1, 1.0)
    assert type(a) == Tuple
    assert str(a) == "<Tuple (4.3, -4.2, 3.1, 1.0)>"


@pytest.mark.parametrize(
    "a, b, expected",
    [
        pytest.param(
            Point(3, -2, 5), Vector(-2, 3, 1), Point(1, 1, 6), id="point-vector"
        ),
        pytest.param(
            Vector(3, -2, 5), Vector(-2, 3, 1), Vector(1, 1, 6), id="vector-vector"
        ),
        pytest.param(
            Color(3, -2, 5), Color(-2, 3, 1), Color(1, 1, 6), id="color-color"
        ),
    ],
)
def test_addition(a, b, expected):
    assert a + b == expected


@pytest.mark.parametrize(
    "a, b, expected",
    [
        pytest.param(
            Point(3, 2, 1), Point(5, 6, 7), Vector(-2, -4, -6), id="point-point"
        ),
        pytest.param(
            Point(3, 2, 1), Vector(5, 6, 7), Point(-2, -4, -6), id="point-vector"
        ),
        pytest.param(
            Vector(3, 2, 1), Vector(5, 6, 7), Vector(-2, -4, -6), id="vector-vector"
        ),
        pytest.param(
            Color(3, 2, 1), Color(5, 6, 7), Color(-2, -4, -6), id="color-color"
        ),
    ],
)
def test_subctraction(a, b, expected):
    assert a - b == expected


@pytest.mark.parametrize(
    "a, expected",
    [
        pytest.param(Point(3, -2, 5), Point(-3, 2, -5), id="point"),
        pytest.param(Vector(3, -2, 5), Vector(-3, 2, -5), id="vector"),
    ],
)
def test_negation(a, expected):
    assert -a == expected


@pytest.mark.parametrize(
    "a, x, expected",
    [
        pytest.param(Point(1, -2, 3), 3.5, Point(3.5, -7, 10.5), id="point"),
        pytest.param(Vector(1, -2, 3), 3.5, Vector(3.5, -7, 10.5), id="vector"),
        pytest.param(Color(1, -2, 3), 3.5, Color(3.5, -7, 10.5), id="color"),
        pytest.param(Point(1, -2, 3), 0.5, Point(0.5, -1, 1.5), id="point-fraction"),
        pytest.param(Vector(1, -2, 3), 0.5, Vector(0.5, -1, 1.5), id="vector-fraction"),
        pytest.param(Color(1, -2, 3), 0.5, Color(0.5, -1, 1.5), id="color-fraction"),
    ],
)
def test_scalar_multiplication(a, x, expected):
    assert a * x == expected
    assert x * a == expected


@pytest.mark.parametrize(
    "a, x, expected",
    [
        pytest.param(Point(1, -2, 3), 2, Point(0.5, -1, 1.5), id="point"),
        pytest.param(Vector(1, -2, 3), 2, Vector(0.5, -1, 1.5), id="vector"),
        pytest.param(Color(1, -2, 3), 2, Color(0.5, -1, 1.5), id="color"),
        pytest.param(Point(1, -2, 3), 0.5, Point(2, -4, 6), id="point-fraction"),
        pytest.param(Vector(1, -2, 3), 0.5, Vector(2, -4, 6), id="vector-fraction"),
        pytest.param(Color(1, -2, 3), 0.5, Color(2, -4, 6), id="color-fraction"),
    ],
)
def test_scalar_division(a, x, expected):
    assert a / x == expected
