import math

from pytracer.tuple import Tuple
from pytracer.vector import Vector


def test_vector():
    a = Vector(4.3, -4.2, 3.1)
    assert str(a) == "<Vector (4.3, -4.2, 3.1)>"
    assert isinstance(a, Tuple)
    assert type(a) == Vector


def test_magnitude():
    a = Vector(1, 0, 0)
    assert math.isclose(a.magnitude(), 1)

    a = Vector(0, 1, 0)
    assert math.isclose(a.magnitude(), 1)

    a = Vector(1, 2, 3)
    assert math.isclose(a.magnitude(), math.sqrt(14))

    a = Vector(-1, -2, -3)
    assert math.isclose(a.magnitude(), math.sqrt(14))


def test_normalization():
    a = Vector(4, 0, 0)
    norm = a.normalized()
    assert norm == Vector(1, 0, 0)
    assert norm.magnitude() == 1

    a = Vector(1, 2, 3)
    norm = a.normalized()
    assert norm == Vector(0.26726, 0.53452, 0.80178)
    assert norm.magnitude() == 1


def test_dotproduct():
    a = Vector(1, 2, 3)
    b = Vector(2, 3, 4)
    assert a.dot(b) == 20


def test_crossproduct():
    a = Vector(1, 2, 3)
    b = Vector(2, 3, 4)
    assert a.cross(b) == Vector(-1, 2, -1)
    assert b.cross(a) == Vector(1, -2, 1)
