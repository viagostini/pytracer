from __future__ import annotations

import math
from itertools import product
from typing import List, Optional, Union

from pytracer.tuple import Tuple


class Matrix:
    def __init__(
        self, size: Optional[int] = None, data: Optional[List[List[float]]] = None
    ):
        if size and not data:
            self.size = size
            self.values = [[0] * size for _ in range(size)]
        elif not size and data:
            self.size = len(data)
            self.values = data
        else:
            raise TypeError

    def __getitem__(self, key: int) -> float:
        return self.values[key]

    def __setitem__(self, key: int, value: float) -> None:
        self.values[key] = value

    def __eq__(self, other: Matrix) -> bool:
        if self.size != other.size:
            return False

        for i, j in product(range(self.size), range(self.size)):
            if not math.isclose(self.values[i][j], other.values[i][j]):
                return False
        return True

    def __mul__(self, other: Matrix) -> Union[Matrix, Tuple]:
        if isinstance(other, Matrix):
            return self.__matrix_mult(self, other)
        if isinstance(other, Tuple):
            return self.__tuple_mult(self, other)

    def __matrix_mult(self, A: Matrix, B: Matrix) -> Matrix:
        ret = [[0] * A.size for _ in range(A.size)]
        for row in range(A.size):
            for col in range(A.size):
                sum = 0
                for index in range(A.size):
                    sum += A[row][index] * B[index][col]
                ret[row][col] = sum
        return Matrix(data=ret)

    def __tuple_mult(self, A: Matrix, B: Tuple) -> Tuple:
        return Tuple(
            Tuple(A[0][0], A[0][1], A[0][2], A[0][3]).dot(B),
            Tuple(A[1][0], A[1][1], A[1][2], A[1][3]).dot(B),
            Tuple(A[2][0], A[2][1], A[2][2], A[2][3]).dot(B),
            Tuple(A[3][0], A[3][1], A[3][2], A[3][3]).dot(B),
        )
