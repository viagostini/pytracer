from __future__ import annotations

import math
import typing

from pytracer.tuple import Tuple


class Color(Tuple):
    def __init__(self, red: float, green: float, blue: float, w: float = 0.0):
        super().__init__(red, green, blue, w)

    @property
    def red(self):
        return self.x

    @property
    def green(self):
        return self.y

    @property
    def blue(self):
        return self.z

    @staticmethod
    def BLACK():
        return Color(0, 0, 0)

    @staticmethod
    def clamp(val: float, min_value: int, max_value: int) -> int:
        scaled_val = math.ceil(val * max_value)
        clamped_val = max(min(max_value, scaled_val), min_value)
        return int(clamped_val)

    def scaled_rgb(
        self, min_value: int = 0, max_value: int = 255
    ) -> typing.Tuple[float, float, float]:
        return (
            Color.clamp(self.red, min_value, max_value),
            Color.clamp(self.green, min_value, max_value),
            Color.clamp(self.blue, min_value, max_value),
        )

    def __iter__(self) -> iter:
        return iter((self.red, self.green, self.blue))

    def __str__(self) -> str:
        return f"<Color ({self.red}, {self.green}, {self.blue})>"

    def __mul__(self, other: typing.Union[float, Color]) -> Color:
        if isinstance(other, Color):
            return Color(
                self.red * other.red, self.green * other.green, self.blue * other.blue
            )
        else:
            return super().__mul__(other)
