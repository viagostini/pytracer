from functools import reduce
from operator import add

from pytracer.canvas import Canvas


class PPMWriter:
    @staticmethod
    def ppm(canvas: Canvas) -> str:
        header = PPMWriter.header(canvas)
        pixels = PPMWriter.pixels(canvas)
        return header + pixels

    @staticmethod
    def header(canvas: Canvas) -> str:
        return f"P3\n{canvas.width} {canvas.height}\n255\n"

    @staticmethod
    def pixels(canvas: Canvas) -> str:
        pixel_data = ""
        for row in canvas.grid:
            ppm_row = reduce(add, [[*col.scaled_rgb()] for col in row])
            for line in [ppm_row[i : i + 17] for i in range(0, len(ppm_row), 17)]:
                pixel_data = pixel_data + " ".join(str(c) for c in line) + "\n"
        return pixel_data
