from typing import Optional

from pytracer.color import Color


class Canvas:
    def __init__(
        self, width: int = 10, height: int = 10, color: Optional[Color] = None
    ):
        self.width = width
        self.height = height

        if not color:
            color = Color.BLACK()

        self.grid = [[color] * width for _ in range(height)]

    def pixel_at(self, row: int, col: int) -> Color:
        return self.grid[col][row]

    def write_pixel_at(self, row: int, col: int, color: Color) -> None:
        self.grid[col][row] = color
