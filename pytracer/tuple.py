from __future__ import annotations

import math

EPSILON = 1e-5


class Tuple:
    def __init__(self, x: float, y: float, z: float, w: float):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __str__(self) -> str:
        return f"<Tuple ({self.x}, {self.y}, {self.z}, {self.w})>"

    def __eq__(self, other: Tuple) -> bool:
        return (
            math.isclose(self.x, other.x, rel_tol=EPSILON)
            and math.isclose(self.y, other.y, rel_tol=EPSILON)
            and math.isclose(self.z, other.z, rel_tol=EPSILON)
            and math.isclose(self.w, other.w, rel_tol=EPSILON)
        )

    def __add__(self, other: Tuple) -> Tuple:
        return self.__class__(
            self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w
        )

    def __sub__(self, other: Tuple) -> Tuple:
        return self.__class__(
            self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w
        )

    def __neg__(self) -> Tuple:
        return self.__class__(-self.x, -self.y, -self.z, self.w)

    def __mul__(self, num: float) -> Tuple:
        return self.__class__(self.x * num, self.y * num, self.z * num, self.w)

    def __rmul__(self, num: float) -> Tuple:
        return self.__mul__(num)

    def __truediv__(self, num: float) -> Tuple:
        return self.__class__(self.x / num, self.y / num, self.z / num, self.w)
