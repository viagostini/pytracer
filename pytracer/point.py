from pytracer.tuple import Tuple


class Point(Tuple):
    def __init__(self, x: float, y: float, z: float, w: float = 1.0):
        super().__init__(x, y, z, w)

    def __str__(self):
        return f"<Point ({self.x}, {self.y}, {self.z})>"
