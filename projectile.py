from __future__ import annotations

from pytracer.canvas import Canvas
from pytracer.color import Color
from pytracer.point import Point
from pytracer.ppm import PPMWriter
from pytracer.vector import Vector


class Environment:
    def __init__(self, gravity: Vector, wind: Vector):
        self.gravity = gravity
        self.wind = wind


class Projectile:
    def __init__(self, position: Point, velocity: Vector):
        self.position = position
        self.velocity = velocity

    def tick(self, environment: Environment) -> Projectile:
        position = self.position + self.velocity
        velocity = self.velocity + environment.gravity + environment.wind
        return Projectile(position, velocity)


if __name__ == "__main__":
    start = Point(0, 1, 0)
    velocity = Vector(1, 1.8, 0).normalized() * 11.25
    proj = Projectile(start, velocity)

    gravity = Vector(0, -0.1, 0)
    wind = Vector(-0.01, 0, 0)
    env = Environment(gravity, wind)

    canvas = Canvas(900, 550)

    red = Color(1, 0, 0)
    while proj.position.y > 0:
        x = int(proj.position.x)
        y = int(canvas.height - proj.position.y)
        canvas.write_pixel_at(x, y, red)

        proj = proj.tick(env)

    with open("images/projectile.ppm", "w") as ppm_file:
        print("\nwriting to file..\n")
        ppm_file.write(PPMWriter.ppm(canvas))
